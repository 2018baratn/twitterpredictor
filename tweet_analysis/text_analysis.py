from textblob import *
import twitter_collect.collect_candidate_tweet_activity as tweet_collect
import twitter_collect.tweet_collect_whole as tweet_collect_main
import twitter_collect.twitter_connection_setup as connect
import twitter_collect.__main__ as main


my_data = tweet_collect_main.collect_to_pandas_dataframe(tweet_collect.get_replies_to_candidate(1976143068, connect.twitter_setup()))


def tweet_extract(data):
    tb = TextBlob(data['Text'].values)   # Extraction du corps des tweets dans la base data
    text = TextBlob(word.lemmatize for word in tb.words)
    return set(text.translate(from_lang="fr", to="en"))  # Renvoie un set (sans doublons) des mots du tweet traduits en anglais


def sort_tweets(data):
    neg_tweets = []
    neu_tweets = []
    pos_tweets = []
    for tweet in data['tweet_textual_content']:
        tb = TextBlob(tweet)
        polarity = tb.translate(to="en").sentiment.polarity
        if polarity < -0.3:
            neg_tweets.append(tweet)
        elif polarity > 0.3:
            pos_tweets.append(tweet)
        else:
            neu_tweets.append(tweet)
    return [pos_tweets, neu_tweets, neg_tweets]


print("Percentage of positive tweets: {}%".format(len(sort_tweets(my_data)[0])*100/len(my_data['tweet_textual_content'])))
print("Percentage of neutral tweets: {}%".format(len(sort_tweets(my_data)[1])*100/len(my_data['tweet_textual_content'])))
print("Percentage de negative tweets: {}%".format(len(sort_tweets(my_data)[2])*100/len(my_data['tweet_textual_content'])))
