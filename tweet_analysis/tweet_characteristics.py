import numpy as np
import twitter_collect.tweet_collect_whole as tweet_collect
import twitter_collect.__main__ as main


data = tweet_collect.collect_to_pandas_dataframe(main.collect())

rt_max = np.max(data['RTs'])   # Renvoie le tweet qui a eu le plus de retweets
rt = data[data.RTs == rt_max].index[0]   # Sélectionne le tweet où le nombre de retweets est maximal

print("The tweet with more retweets is: \n{}".format(data['tweet_textual_content'][rt]))   # On renvoie l'information sous forme d'une phrase en français
print("Number of retweets: {}".format(rt_max))
print("{} characters.\n".format(data['len'][rt]))


rt_min = np.min(data['RTs'])   # Renvoie le tweet qui a eu le moins de retweets
rt = data[data.RTs == rt_min].index[0]   # Sélectionne le tweet où le nombre de retweets est minimal

print("The tweet with less retweets is: \n{}".format(data['tweet_textual_content'][rt]))   # On renvoie l'information sous forme d'une phrase en français
print("Number of retweets: {}".format(rt_min))
print("{} characters.\n".format(data['len'][rt]))


max_likes = np.max(data['Likes'])   # Renvoie le tweet qui a eu le plus de likes
tweet_max_likes = data[data.Likes == max_likes].index[0]   # Sélectionne le tweet où le nombre de likes est maximal

print("The tweet with more likes is: \n{}".format(data['tweet_textual_content'][tweet_max_likes]))   # On renvoie l'information sous forme d'une phrase en français
print("Number of likes: {}".format(max_likes))
print("{} characters.\n".format(data['len'][tweet_max_likes]))


min_likes = np.min(data['Likes'])   # Renvoie le tweet qui a eu le moins de likes
tweet_min_likes = data[data.Likes == min_likes].index[0]   # Sélectionne le tweet où le nombre de likes est minimal

print("The tweet with less likes is: \n{}".format(data['tweet_textual_content'][tweet_min_likes]))   # On renvoie l'information sous forme d'une phrase en français
print("Number of likes: {}".format(min_likes))
print("{} characters.\n".format(data['len'][tweet_min_likes]))
