import pandas as pd
import matplotlib.pyplot as plt
import twitter_collect.tweet_collect_whole as tweet_collect
import twitter_collect.__main__ as main


data = tweet_collect.collect_to_pandas_dataframe(main.collect())


# Comparaison entre les retweets et les likes

fav = pd.Series(data=data['Likes'].values, index=data['Date'])
ret = pd.Series(data=data['RTs'].values, index=data['Date'])

# On renvoie le résultat sous forme de graphe

fav.plot(figsize=(16,4), label="Likes", legend=True)
ret.plot(figsize=(16,4), label="Retweets", legend=True)

plt.show()


# Comparaison entre les réponses et les likes

fav = pd.Series(data=data['Likes'].values, index=data['Date'])
rep = pd.Series(data=data['Replies'].values, index=data['Date'])

# On renvoie le résultat sous forme de graphe

fav.plot(figsize=(16,4), label="Likes", legend=True)
ret.plot(figsize=(16,4), label="Replies", legend=True)

plt.show()


# Comparaison entre les retweets et les réponses

rep = pd.Series(data=data['Replies'].values, index=data['Date'])
ret = pd.Series(data=data['RTs'].values, index=data['Date'])

# On renvoie le résultat sous forme de graphe

fav.plot(figsize=(16,4), label="Replies", legend=True)
ret.plot(figsize=(16,4), label="Retweets", legend=True)

plt.show()
