import tweepy
import twitter_collect.twitter_connection_setup as connect


def get_replies_to_candidate(num_candidate, twitter_api):   # Renvoie l'ensemble des réponses aux tweets d'un candidat (identifié par son ID)
    statuses = twitter_api.user_timeline(id=num_candidate, count=50)
    return statuses


def get_retweets_of_candidate(num_candidate, twitter_api):   # Renvoie l'ensemble des reweets des tweets d'un candidat donné
    tweets = twitter_api.user_timeline(id=num_candidate, count=10)
    retweets = []
    for tweet in tweets:
        retweets += twitter_api.retweets(tweet.id)
    return retweets


#print(get_replies_to_candidate(1976143068, connect.twitter_setup()))
