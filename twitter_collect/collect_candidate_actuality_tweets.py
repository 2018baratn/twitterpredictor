import tweepy
from twitter_collect.twitter_connection_setup import *
from twitter_collect.collect_candidate_tweet_activity import *


def get_candidate_queries(num_candidate, file_path, file_category):
    """
    Generate and return a list of string queries for the search Twitter API from the file file_path_num_candidate.txt
    :param num_candidate: the number of the candidate
    :param file_path: the path to the keyword and hashtag files
    :param file_category: type of the keyword, either "keywords" or "hashtags"
    :return: (list) a list of string queries that can be done to the search API independently
    """
    try:
        with open("{}/{}_candidate_{}.txt".format(file_path, file_category, num_candidate), 'r') as file:   # Reads the file
            keywords = file.read().split("\n")    # Finds the keywords related to a candidate in the file and returns them
        return keywords
    except IOError:   # In the case an error occures
        return []


def get_tweets_from_candidates_search_queries(queries, twitter_api):
    tweets = []
    try:
        for query in queries:   # For each query, returns the tweets containing this query
            tweets.append(twitter_api.search(query, language="french", rpp=100))
    except tweepy.TweepError as TweepError:
        print("You raised a TweepError exception with code : "+TweepError.response.text)  # prints the TweepError 
    return tweets


get_tweets_from_candidates_search_queries(
        get_candidate_queries(1976143068, "C:/Users/nyco-/PycharmProjects/twitterPredictor/CandidateData", "keywords"),
        twitter_setup())   # Test with Emmanuel Macron's ID
