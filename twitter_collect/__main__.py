from twitter_collect.twitter_connection_setup import *
from twitter_collect.collect_candidate_tweet_activity import *
from twitter_collect.collect_candidate_actuality_tweets import *
import tweepy


api = twitter_setup()


def collect():
    list_of_tweets = []
    for tweet in get_tweets_from_candidates_search_queries(
            get_candidate_queries(1976143068, "C:/Users/nyco-/PycharmProjects/twitterPredictor/CandidateData", "keywords"),
            api):
        list_of_tweets.append(tweet)
    for tweet in get_replies_to_candidate(
            1976143068,
            api):
        list_of_tweets.append(tweet)
    for tweet in get_retweets_of_candidate(
            1976143068,
            api):
        list_of_tweets.append(tweet)
    return list_of_tweets
