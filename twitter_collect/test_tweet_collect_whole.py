from pytest import *
from twitter_collect.__main__ import collect
from twitter_collect.tweet_collect_whole import collect_to_pandas_dataframe


def test_collect():
    tweets = collect()
    data = collect_to_pandas_dataframe(tweets)
    assert 'tweet_textual_content' in data.columns
