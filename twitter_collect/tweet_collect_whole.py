import json
import pandas as pd
import numpy as np


def store_tweets(tweets, filename):
    tweets_all = []   # Liste vide qu'on remplit au fur et à mesure avec les tweets
    for tweet in tweets:
        t = {}   # On crée une structure t qui accueillera le tweet
        t["id"] = tweet.id
        t["text"] = tweet.text
        t["date"] = str(tweet.created_at)
        t["author"] = tweet.user.name
        tweets_all.append(t)
    with open("C:/Users/nyco-/PycharmProjects/twitterPredictor/TweetStorage/{}".format(filename), 'w', encoding='utf-8') as file:
        json.dump(tweets_all, file, indent=4)


#store_tweets(get_retweets_of_candidate(1976143068, twitter_setup()), "candidate_1976143068.txt")


"""def transform_to_dataframe(status):
    df = pd.DataFrame({'id': status.id,
                       'Text': status.text,
                       'Date': status.created_at,
                       'Author': status.user.name},
                      index=[status.id])
    return df"""


def collect_to_pandas_dataframe(tweets):   # Ici, une seule structure accueille tous les tweets, au lieu d'en créer une par tweet
    data = pd.DataFrame(data=[tweet.text for tweet in tweets], columns=['tweet_textual_content'])
    data['len'] = np.array([len(tweet.text) for tweet in tweets])
    data['ID'] = np.array([tweet.id for tweet in tweets])
    data['Date'] = np.array([tweet.created_at for tweet in tweets])
    data['Source'] = np.array([tweet.source for tweet in tweets])
    data['Likes'] = np.array([tweet.favorite_count for tweet in tweets])
    data['RTs'] = np.array([tweet.retweet_count for tweet in tweets])
    return data
